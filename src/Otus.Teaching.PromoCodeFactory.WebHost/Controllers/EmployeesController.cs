﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        //Create/Delete/Update

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        ///
        [HttpPost]
        public async Task<ActionResult<Employee>> CreateEmployee(EmployeeCreation employee)
        {
            var fakeDataRoles = FakeDataFactory.Roles;
            var rolesString = employee.Roles;
            List<Role> foundRole = fakeDataRoles.Where(x => rolesString.Any(r => x.Name.ToLower() == r.ToLower())).ToList();

            var newEmployee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Roles = foundRole
            };

            return await _employeeRepository.Add(newEmployee);

        }


        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        ///
        [HttpDelete("{userId}")]
        public void DeleteEmployee(Guid userId)
        {

            var foundUser = FakeDataFactory.Employees.FirstOrDefault(e => e.Id == userId);

            if (foundUser == null) NotFound("User not found");

            _employeeRepository.Remove(userId);
        }


        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        [HttpPut("{userId}")]
        public async Task<ActionResult<Employee>> EditEmployee(Guid userId, EmployeeCreation employee)
        {
            var findUser = GetEmployeeByIdAsync(userId);

            if (findUser == null) return NotFound("User not found");

            var fakeDataRoles = FakeDataFactory.Roles;
            var rolesString = employee.Roles;
            List<Role> foundRole = fakeDataRoles.Where(x => rolesString.Any(r => x.Name.ToLower() == r.ToLower())).ToList();

            var tmpUser = new Employee
            {
                Id = userId,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                Roles = foundRole,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return await _employeeRepository.Edit(tmpUser);
        }
    }
}

