﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;

        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Add(T obj)
        {

            Data.Add(obj);

            return Task.FromResult(Data.FirstOrDefault(x => x.Id == obj.Id));

        }

        public void Remove(Guid id)
        {
            var findUser = Data.FirstOrDefault(x => x.Id == id);

            if (findUser != null) Data.Remove(findUser);
        }

        public Task<T> Edit(T obj)
        {
            var findUser = Data.First(x => x.Id == obj.Id) as Employee;
            var editedUser = (obj as Employee);

            findUser.FirstName = editedUser.FirstName;
            findUser.LastName = editedUser.LastName;
            findUser.Email = editedUser.Email;
            findUser.AppliedPromocodesCount = editedUser.AppliedPromocodesCount;
            findUser.Roles = editedUser.Roles;


            return Task.FromResult(Data.FirstOrDefault(x => x.Id == obj.Id));
        }
    }
}